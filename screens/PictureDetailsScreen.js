import React from 'react';
import {
  View,
  Text,
  Button,
  Image,
  Alert,
  StyleSheet,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';

import Colors from '../constants/Colors';

/** Renders the result screen once the data was received */
const PictureDetails = ({ navigation }) => {
  const results = navigation.getParam('results');
  const image = navigation.getParam('image');

  /* eslint-disable */
  console.log(results);
  const {
    Name, Result, CertaintyFruit, CertaintyFresh,
  } = results;

  let isSaveable = false;

  // Determines what will be displayed to the user depending on the results
  const resultDisplay = {
    title: '',
    accuracy: '',
    freshness: '',
    fruitColor: Colors.failedResult,
    accuracyColor: Colors.failedResult,
    freshnessColor: Colors.failedResult,
  };

  switch ((Name || '').toLowerCase()) {
    case 'none':
      resultDisplay.title = 'Your fruit was not recognized';
      resultDisplay.accuracy = 'Make sure your fruit is within the frame';
      resultDisplay.freshness = '';
      break;
    case 'offline':
    case '':
      resultDisplay.title = 'The server is currently offline';
      resultDisplay.accuracy = 'Please try again later';
      resultDisplay.freshness = '';
      break;
    default:
      resultDisplay.isAn = ['a', 'o', 'i', 'u', 'y', 'e'].includes(Name.substring(0, 1));
      resultDisplay.title = 'This fruit is '
        + `${resultDisplay.isAn ? 'an' : 'a'} ${Name} (${CertaintyFruit}%)`;
      resultDisplay.fruitColor = Colors.primary;

      // Handles display in freshness area (colors/text)
      if (Result.toLowerCase() === 'none') {
        resultDisplay.accuracy = 'You may need to take a clearer picture.';
        resultDisplay.freshness = 'Unable to the determine freshness level.';
      } else {
        resultDisplay.accuracy = `Accuracy of prediction: ${CertaintyFresh}%`;
        resultDisplay.freshness = `Freshness level : ${Result}`;
        resultDisplay.accuracyColor = Colors.primary;
        if (Result.toLowerCase() === 'rotten') {
          resultDisplay.freshnessColor = Colors.rottenResult;
        } else {
          resultDisplay.freshnessColor = Colors.freshResult;
        }
      }

      // Enables the save button when accounts will be addded
      isSaveable = true;
      break;
  }

  return (
    <View style={styles.content}>
      <View style={styles.titleContainer}>
        <Text style={{ ...styles.title, color: resultDisplay.fruitColor }}>
          {resultDisplay.title}
        </Text>
      </View>
      <View style={styles.imagePreview}>
        <Image
          style={styles.image}
          source={{ uri: image }}
        />
      </View>
      <View style={styles.textContainer}>
        <Text style={{ ...styles.text, color: resultDisplay.freshnessColor }}>
          {resultDisplay.freshness}
        </Text>
        <Text style={{ ...styles.text, color: resultDisplay.accuracyColor }}>
          {resultDisplay.accuracy}
        </Text>
      </View>
      <View style={styles.actionContainer}>
        <View style={styles.action}>
          <Button title="Back" onPress={() => navigation.goBack()} />
        </View>
        <View style={styles.action}>
          <Button
            title="Save"
            disabled={!isSaveable}
            onPress={() => {
              Alert.alert(
                'Unimplemented',
                'Accounts are not enabled as of right now, the save operation will be enabled in a future version',
                [{ text: 'OK' }],
              );
            }}
          />
        </View>
      </View>
    </View>
  );
};

PictureDetails.propTypes = {
  navigation: PropTypes.any.isRequired,
};

const styles = StyleSheet.create({
  action: {
    width: 100,
  },
  actionContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    width: 250,
  },
  content: {
    alignItems: 'center',
    flex: 1,
    marginTop: 25,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  imagePreview: {
    alignItems: 'center',
    borderColor: Colors.gray,
    borderWidth: 1,
    height: Dimensions.get('window').width * 0.60,
    justifyContent: 'center',
    marginBottom: 10,
    width: Dimensions.get('window').width * 0.60,
  },
  text: {
    fontFamily: 'poppins-regular',
    fontSize: 18,
    textAlign: 'center',
  },
  textContainer: {
    marginVertical: 15,
  },
  title: {
    fontFamily: 'poppins-regular',
    fontSize: 21,
    textAlign: 'center',
  },
  titleContainer: {
    marginBottom: 20,
  },
});

export default PictureDetails;
