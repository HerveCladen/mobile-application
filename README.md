# Mobile Application

Front-end part of the hybrid native app

## Instructions
Note: Expo cli may not work with the most recent version of node (LTS or Dev). This project was made with node 10.* to 12.9*<br />
Note: To run on an emulator, it needs to be opened before the command to open on the emulator is sent. <br />

 1- Install expo cli globally with ```npm install -g expo-cli``` <br />
 2- In the project folder, install dependencies with ```npm install``` <br />
 3- To run the project: ```npm run start``` <br />
 4- Download the Expo client app on the emulator or your smartphone <br />
 5- Use the platform <br />
    a) Emulator: in the metro bundler you can click run on Android emulator and it will detect the running emulator automatically if the Expo client app is installed on it. <br />
    b) Physical Smartphone: Scan the QR code from within the Projects section in the Expo Client app (Android) or from the phone's camera (iOS) <br />

Note: To receive data while you are using the app, the backend must be running on your local network. <br />
Note: This project was tested and debugged on Android. <br />
