/** @class Notification defines all the data needed to define a notification to display it it accordingly on the main page */
class Notification {
  constructor(id, title, body, type, severity, sentDate, expirationDate, isDeletable, scope) {
    this.id = id;
    this.body = body;
    this.type = type;
    this.severity = severity;
    this.sentDate = sentDate;
    this.expirationDate = expirationDate;
    this.scope = scope;
    this.title = title;
    this.isDeletable = isDeletable;
  }
}

export default Notification;
