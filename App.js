import React, { useState } from 'react';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { enableScreens } from 'react-native-screens';

import MainNavigator from './navigation/MainNavigator';

enableScreens();

/** Fetches all the fonts needed before the app loads */
const fetchFonts = () => Font.loadAsync({
  'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
  'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  'audiowide-regular': require('./assets/fonts/Audiowide-Regular.ttf'),
  'arsenal-regular': require('./assets/fonts/Arsenal-Regular.ttf'),
  'poppins-regular': require('./assets/fonts/Poppins-Regular.ttf'),
  'poppins-bold': require('./assets/fonts/Poppins-Bold.ttf'),
});


/** Calls all fetch functions to load the app */
const fetchAll = async () => {
  await fetchFonts();
};

export default function App() {
  const [isLoaded, setIsLoaded] = useState(false);

  // Waits for everything to be fetched
  if (!isLoaded) {
    return <AppLoading startAsync={fetchAll} onFinish={() => setIsLoaded(true)} />;
  }

  // Once loaded, render the app
  return (
    <MainNavigator />
  );
}
